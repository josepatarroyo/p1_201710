import model.logic.SistemaRecomendacionPeliculas;
import junit.framework.TestCase;


public class TestSistemaRecomendacion extends TestCase
{
	
	private SistemaRecomendacionPeliculas recomendador;
	
	public void testCargarPeliculasSR()
	{
		recomendador = new SistemaRecomendacionPeliculas();
		recomendador.cargarPeliculasSR(SistemaRecomendacionPeliculas.RUTA_PELICULAS);
		assertNotNull("No se cre� la lista de peliculas", recomendador.darPelis());
		assertEquals("pelis es vac�o" , recomendador.darPelis().darNumeroElementos() == 0);
		assertNotNull("No se cre� la lista de g�neros", recomendador.darGeneros());
		assertNotNull("No se cre� el cat�logo pel�culas ratings", recomendador.darCatalogoPeliculaRating());
		assertNotNull("No se cre� el cat�logo generos pel�culas", recomendador.darCatalogoGeneroPeliculas());
		assertNotNull("No se cre� la lista de g�neros", recomendador.darGeneros());
	}
}
