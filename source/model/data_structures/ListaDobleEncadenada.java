package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private NodoDoblementeEncadenado<T> primero;

	private NodoDoblementeEncadenado<T> ultimo;
	
	//representa la posicion actual de la lista, para desplazarse por ella
	private int nodoActual = 0;

	private int numeroDeNodos = 0;

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub

		Iterator<T> iter =  new Iterator<T>() {

			int actualLocal = 0;

			@Override
			public boolean hasNext()
			{
				// TODo Auto-generated method stub

				return actualLocal < numeroDeNodos;
			}

			@Override
			public T next() {
				// TODo Auto-generated method stub

				actualLocal++;
				return darElemento(actualLocal-1);
			}
			
			@Override
			public void remove()
			{
				
			}
		};

		return iter;
	}
	public ListaDobleEncadenada(NodoDoblementeEncadenado<T> primero, NodoDoblementeEncadenado<T> ultimo, int tamanioLista)
	{
		this.primero = primero;
		this.ultimo = ultimo;
		numeroDeNodos = tamanioLista;
		nodoActual = 0;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		
		if (primero == null) //si la lista est� vac�a, se inicializa
		{
			primero = new NodoDoblementeEncadenado<T>(null, null, elem);
			ultimo = primero;
			
			numeroDeNodos = 1;
		}
		else
		{			
			//Creo un nodo que el anterior sea el �ltimo, el siguiente nullo y su elemento sea
			//el que entr� por par�metro
			
			NodoDoblementeEncadenado<T> nodoFinal = new NodoDoblementeEncadenado<T>(ultimo, null, elem);
			
			//Cambio al �ltimo su siguiente, para que sea el nuevo
			ultimo.cambiarSiguiente(nodoFinal);
			
			//Ahora el nodo nuevo es el �ltimo
			ultimo = nodoFinal;
			
			numeroDeNodos ++;
		}

	}

	@Override
	public T darElemento(int pos) {
		return darNodoPosicion(pos).darElemento();
	}
	
	public NodoDoblementeEncadenado<T> darNodoPosicion(int pos) {
		// TODO Auto-generated method stub

		int i =0;
		NodoDoblementeEncadenado<T> elem = null;

		if (primero!= null)
		{
			NodoDoblementeEncadenado<T> actual = null;
			
			elem = primero;
			actual = primero;

			while (i < pos)
			{
				actual = actual.darSiguiente();

				i++;
			}
			
			elem = actual;
		}


		return elem;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return numeroDeNodos;
	}

	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return darElemento(nodoActual);
	}

	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		
		boolean puedoAvanzar = true;
		
		if(nodoActual + 1 == numeroDeNodos) 
		//Si el siguiente al actual es igual el n�mero de nodos, no existe dicho elemento, entonces, no puedo avanzar a dicha posicion
		{
			puedoAvanzar = false;
		}
		else
		{
			nodoActual++;
		}
		
		return puedoAvanzar;
	}

	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		
		boolean puedoRetroceder = true;
		
		if (nodoActual-1 <0)
		//Si el anterior a nodo actual es negativo, entonces no puedo retroceder
		{
			puedoRetroceder = false;
		}
		else
		{
			nodoActual--;
		}
		
		return puedoRetroceder;
	}

	public String toString()
	{
		String respuesta = "[";
		
		Iterator<T> iter = iterator();
		
		while (iter.hasNext())
		{
			respuesta = respuesta + iter.next().toString();
			
			if(iter.hasNext())
				respuesta = respuesta +", ";
		}
		
		
		return respuesta + "]";
	}

	public void cambiarElementoEn(int pos, T elem) {
		
		int contador = 0;
		NodoDoblementeEncadenado<T> actual = primero;
		
		while (contador < pos-1)
		{
			actual = actual.darSiguiente();
			
			contador++;
		}
		
		 actual.darSiguiente().cambiarElemento(elem);		
	}

	public ILista<T> partirPorLaMitad() 
	{
		int mitad = numeroDeNodos / 2 - 1;
		NodoDoblementeEncadenado<T> mitadLista = darNodoPosicion(mitad);
		ListaDobleEncadenada<T> segundaMitad = new ListaDobleEncadenada<>(mitadLista.darSiguiente(), ultimo, numeroDeNodos-mitad-1);
		numeroDeNodos = mitad +1;
		if(nodoActual > mitad)
		{
			nodoActual = mitad;
		}
		ultimo = mitadLista;
		ultimo.cambiarSiguiente(null);
		return segundaMitad;
	}
	@Override
	public T eliminarElemento(int pos) 
	{
		return null;
	}
}

