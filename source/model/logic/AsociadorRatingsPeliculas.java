package model.logic;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VORating;

public class AsociadorRatingsPeliculas 
{
	ListaEncadenada<VOPelicula> peliculas;
	ListaEncadenada<VORating> ratings;
	ListaEncadenada<VOGeneroPelicula> generos;

	public AsociadorRatingsPeliculas(ListaEncadenada<VOPelicula> peliculas, ListaEncadenada<VORating> ratings , ListaEncadenada<VOGeneroPelicula> generos)
	{
		this.peliculas = peliculas;
		this.ratings = ratings;
		this.generos = generos;
		asociar();
	}

	public void asociar()
	{
		NodoEncadenado<VORating> ratingActual = ratings.darNodoPosicion(0);
		while(ratingActual.darSiguiente() != null)
		{
			NodoEncadenado<VOGeneroPelicula> actualGenero = generos.darNodoPosicion(0);
			while(actualGenero.darSiguiente() != null)
			{
				ListaEncadenada<VOPelicula> lPelis = (ListaEncadenada<VOPelicula>) actualGenero.darElemento().getPeliculas();
				NodoEncadenado<VOPelicula> peliculaActual = lPelis.darNodoPosicion(0);
				boolean agregado = false;
				while(peliculaActual.darSiguiente() != null && !agregado)
				{
					if(ratingActual.darElemento().getIdPelicula() == peliculaActual.darElemento().getId())
					{
						double r = (peliculaActual.darElemento().getPromedioRatings() + ratingActual.darElemento().getRating())/2;
						peliculaActual.darElemento().setPromedioRatings(r);
					}
					peliculaActual = peliculaActual.darSiguiente();
				}
				actualGenero = actualGenero.darSiguiente();
			}
			ratingActual = ratingActual.darSiguiente();
		}
	}

	public ListaEncadenada<VOGeneroPelicula> darGeneros()
	{
		return generos;
	}

}
