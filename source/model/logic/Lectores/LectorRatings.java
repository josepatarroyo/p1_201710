package model.logic.Lectores;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VORating;
import model.vo.VOUsuario;

public class LectorRatings 
{
	private String ruta;
	ListaEncadenada<VORating> ratings;
	ListaEncadenada<VOUsuario> usuarios;

	public LectorRatings(String pRuta)
	{
		ruta = pRuta;
		ratings = new ListaEncadenada<VORating>();
		usuarios = new ListaEncadenada<VOUsuario>();
		leerArchivo();
	}

	public void leerArchivo()
	{
		try
		{
			FileReader fr = new FileReader(ruta);
			BufferedReader lector = new BufferedReader(fr);
			String lineaActual;
			lineaActual = lector.readLine();
			lineaActual = lector.readLine();
			while(lineaActual != null)
			{
				String[] datos = lineaActual.split(",");
				long userID = Long.parseLong(datos[0]);
				long movieID = Long.parseLong(datos[1]);
				double rating = Double.parseDouble(datos[2]);
				long timeStamp = Long.parseLong(datos[0]);
				VORating nuevo = new VORating();
				VOUsuario nuevoUsuario = new VOUsuario();

				nuevoUsuario.setIdUsuario(userID);
				nuevo.setIdUsuario(userID);
				nuevo.setIdPelicula(movieID);
				nuevo.setRating(rating);
				nuevo.setTimeStamp(timeStamp);
				nuevoUsuario.setPrimerTimestamp(timeStamp);
				ratings.agregarElementoFinal(nuevo);
				manejarUsuario(nuevoUsuario);
				lineaActual = lector.readLine();
			}
			//out();
			lector.close();
			fr.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void out()
	{
		NodoEncadenado<VORating> actual = ratings.darNodoPosicion(0);
		while(actual.darSiguiente() != null)
		{
			System.out.println("-----------------------------------------------");
			System.out.println("Id: " + actual.darElemento().getIdPelicula());
			System.out.println("Rating: " + actual.darElemento().getRating());
			System.out.println("Usuario: " + actual.darElemento().getIdUsuario());
			actual = actual.darSiguiente();
		}

		NodoEncadenado<VOUsuario> actualUsuario = usuarios.darNodoPosicion(0);
		while(actualUsuario.darSiguiente() != null)
		{
			System.out.println("-----------------------------------------------");
			System.out.println("Id Usuario: " + actualUsuario.darElemento().getIdUsuario());
			System.out.println("N�mero Ratings: " + actualUsuario.darElemento().getNumRatings());
			actualUsuario = actualUsuario.darSiguiente();
		}

	}

	public void eliminarUsuariosRepetidos()
	{
		System.out.println("ELIMINANDO USUARIOS REPETIDOS: ");

		for(int i=0; i < usuarios.darNumeroElementos()-1; i++)
		{
			NodoEncadenado<VOUsuario> gi = usuarios.darNodoPosicion(i);
			for(int j=i+1; j < usuarios.darNumeroElementos(); j++)
			{
				NodoEncadenado<VOUsuario> gj = usuarios.darNodoPosicion(j);
				if(gj.darElemento().getIdUsuario() == gi.darElemento().getIdUsuario())
				{
					usuarios.eliminarElemento(j);
					System.out.println(usuarios.darElemento(j).getIdUsuario());
				}
			}
		}
	}
	
	public void manejarUsuario(VOUsuario u)
	{
		if(usuarios.darNumeroElementos() == 0)
		{
			usuarios.agregarElementoFinal(u);
			System.out.println("AGREGADO PRIMERO");
		}
		else
		{
			boolean seDebeActualizar = false;
			int posicion = -1;
			for(int i=0; i < usuarios.darNumeroElementos(); i++)
			{
				VOUsuario aComparar = usuarios.darElemento(i);
				if(u.getIdUsuario() == aComparar.getIdUsuario())
				{
					seDebeActualizar = true;
					posicion = i;
				}
			}
			if(seDebeActualizar)
			{
				usuarios.darElemento(posicion).sumarRating();
				//System.out.println("ACTUALIZADO");
			}
			else
			{
				usuarios.agregarElementoFinal(u);
				//System.out.println("AGREGADO");
			}
			
		}
	}


	public ListaEncadenada<VORating> darRatings()
	{
		return ratings;
	}

	public ListaEncadenada<VOUsuario> darUsuarios()
	{
		return usuarios;
	}


}	
