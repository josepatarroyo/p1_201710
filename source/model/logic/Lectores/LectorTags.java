package model.logic.Lectores;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.ListaEncadenada;
import model.vo.VOTag;

public class LectorTags 
{
	private String ruta;
	private ListaEncadenada<VOTag> tags;

	public LectorTags(String pRuta)
	{
		ruta = pRuta;
		tags = new ListaEncadenada<VOTag>();
		leerArchivo();
	}

	public void leerArchivo()
	{
		try
		{
			FileReader fr = new FileReader(ruta);
			BufferedReader lector = new BufferedReader(fr);
			String lineaActual;
			lineaActual = lector.readLine();
			lineaActual = lector.readLine();
			while(lineaActual != null)
			{

				String[] datos = lineaActual.split(",");
				long userID = Long.parseLong(datos[0]);
				long movieID = Long.parseLong(datos[1]);
				String tag = datos[2];
				long timeStamp = Long.parseLong(datos[0]);
				VOTag nuevo = new VOTag();
				nuevo.setUserId(userID);
				nuevo.setMovieId(movieID);
				nuevo.setTag(tag);
				nuevo.setTimestamp(timeStamp);
				tags.agregarElementoFinal(nuevo);
				lineaActual = lector.readLine();
			}
			//out();
			lector.close();
			fr.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}
	
	public void out()
	{
		for(int i=0; i < tags.darNumeroElementos(); i++)
		{
			System.out.println(tags.darElemento(i).getTag());
		}
	}

	public ListaEncadenada<VOTag> darTags()
	{
		return tags;
	}
	
}
