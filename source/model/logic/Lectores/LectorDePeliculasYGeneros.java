package model.logic.Lectores;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Iterator;

import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoDoblementeEncadenado;
import model.data_structures.NodoEncadenado;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class LectorDePeliculasYGeneros 
{
	private ListaEncadenada<VOPelicula> peliculas;
	private ListaEncadenada<VOGeneroPelicula> generos;
	private String ruta;

	public LectorDePeliculasYGeneros(String pRuta)
	{
		ruta = pRuta;
		peliculas = new ListaEncadenada<VOPelicula>();
		generos = new ListaEncadenada<VOGeneroPelicula>();
		leerArchivoPeliculas();
	}

	public void leerArchivoPeliculas()
	{
		try
		{
			FileReader fr = new FileReader(ruta);
			BufferedReader lector = new BufferedReader(fr);
			String lineaActual;
			lineaActual = lector.readLine();
			lineaActual = lector.readLine();
			char c = '"';
			while(lineaActual != null)
			{
				ListaEncadenada<VOPelicula> pelis = new ListaEncadenada<VOPelicula>();
				try
				{
					String[] datos = new String[4];
					String id = "";
					String nombre = "";
					String anio = "";
					String generos = "";

					boolean quotes = false;
					for(int i=0; i < lineaActual.length(); i++)
					{
						if(lineaActual.charAt(i) == '"' && !quotes)
						{
							quotes = true;
						}
					}
					if(quotes == true)
					{
						String[] division = lineaActual.split(Character.toString(c));
						String parteSinComas = division[1].replace(',', '$');
						division[1] = parteSinComas;
						lineaActual = division[0]+division[1]+division[2];
					}

					int cantidadParentesis = 0;
					for(int i=0; i < lineaActual.length(); i++)
					{
						if(lineaActual.charAt(i)== '(' || lineaActual.charAt(i) == ')')
						{
							cantidadParentesis++;
						}
					}

					if(cantidadParentesis == 4)
					{
						lineaActual = lineaActual.replace('(', '%');
						lineaActual = lineaActual.replace(')', '%');
						String[] partes = lineaActual.split("%");
						id = partes[0].split(",")[0];
						nombre = partes[0].split(",")[1] + partes[1];
						anio = partes[3];
						generos = partes[4].replace(',', '+');

						nombre = nombre.replace('$', ',');
						datos[0] = id;
						datos[1] = nombre;
						datos[2] = anio;
						datos[3] = generos;
						agregarAListaPeliculasNormales(datos);
						lineaActual = lector.readLine();
					}
					else if(cantidadParentesis == 2)
					{
						lineaActual = lineaActual.replace('(','�');
						lineaActual = lineaActual.replace(')' , '�');
						String[] partes = lineaActual.split(",");
						id = partes[0];
						nombre = partes[1].split("�")[0];
						anio = partes[1].split("�")[1];
						generos = partes[2];

						nombre = nombre.replace('$', ',');
						datos[0] = id;
						datos[1] = nombre;
						datos[2] = anio;
						datos[3] = generos;
						agregarAListaPeliculasNormales(datos);
						lineaActual = lector.readLine();
					}
					else
					{
						lineaActual = lector.readLine();
					}
				}
				catch(Exception e)
				{
					
				}

			}
			agregarPeliculasAGeneros();
			//out();
			lector.close();
			fr.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public void agregarAListaPeliculasNormales(String[] datos)
	{
		if(datos != null)
		{
			try
			{
				VOPelicula nueva = new VOPelicula();

				String nombre = datos[1];
				nueva.setTitulo(nombre);

				String anio = datos[2];
				if(anio.contains("-"))
				{

					anio = anio.split("-")[0];
					int anho = Integer.parseInt(anio);
					nueva.setAgnoPublicacion(anho);

				}
				else
				{
					int anho = Integer.parseInt(anio);
					nueva.setAgnoPublicacion(anho);
				}

				Long id = Long.parseLong(datos[0]);
				nueva.setId(id);

				String rawGeneros = datos[3];
				String[] arregloGeneros = rawGeneros.split("\\|");
				ListaEncadenada<String> generosString = new ListaEncadenada<String>();
				ListaEncadenada<VOGeneroPelicula> generos = new ListaEncadenada<VOGeneroPelicula>();
				for(int i = 0; i < arregloGeneros.length; i++)
				{
					VOGeneroPelicula nuevo = new VOGeneroPelicula();
					nuevo.setGenero(arregloGeneros[i]);
					generosString.agregarElementoFinal(arregloGeneros[i]);
					generos.agregarElementoFinal(nuevo);
				}
				nueva.setGenerosAsociados(generosString);
				agregarAListaDeGeneros(generos);
				peliculas.agregarElementoFinal(nueva);
			}
			catch(Exception e)
			{

			}
		}
		else
		{
			System.out.println("WTF");
		}
	}

	public void agregarAListaDeGeneros(ListaEncadenada<VOGeneroPelicula> lista)
	{
		NodoEncadenado<VOGeneroPelicula> actual = lista.darNodoPosicion(0);
		while(actual.darSiguiente() != null)
		{
			generos.agregarElementoFinal(actual.darElemento());
			actual = actual.darSiguiente();
		}
		eliminarGenerosRepetidos();
	}

	public void eliminarGenerosRepetidos()
	{
		for(int i=0; i < generos.darNumeroElementos()-1; i++)
		{
			NodoEncadenado<VOGeneroPelicula> gi = generos.darNodoPosicion(i);
			for(int j=i+1; j < generos.darNumeroElementos(); j++)
			{
				NodoEncadenado<VOGeneroPelicula> gj = generos.darNodoPosicion(j);
				if(gj.darElemento().getGenero().equals(gi.darElemento().getGenero()))
				{
					generos.eliminarElemento(j);
				}
				else if(gj.darElemento().getGenero().charAt(0) == '+')
				{
					generos.eliminarElemento(j);
				}
			}
		}
	}


	public ListaEncadenada<VOPelicula> darPeliculas()
	{
		return peliculas;
	}

	public ListaEncadenada<VOGeneroPelicula> darGeneros()
	{
		return generos;
	}





	public void agregarPeliculasAGeneros()
	{
		NodoEncadenado<VOPelicula> actualPeli = peliculas.darNodoPosicion(0);
		while(actualPeli.darSiguiente() != null)
		{
			ListaEncadenada<String> actualGeneroPeli = (ListaEncadenada<String>) actualPeli.darElemento().getGenerosAsociados();
			NodoEncadenado<String> generoDePeli = actualGeneroPeli.darNodoPosicion(0);
			while(generoDePeli.darSiguiente() != null)
			{
				NodoEncadenado<VOGeneroPelicula> gpa = generos.darNodoPosicion(0);
				while(gpa.darSiguiente() != null)
				{
					if(gpa.darElemento().getGenero().equals(generoDePeli.darElemento()))
					{
						if(gpa.darElemento().getPeliculas() == null)
						{
							gpa.darElemento().setPeliculas(new ListaEncadenada<VOPelicula>());
						}
						gpa.darElemento().getPeliculas().agregarElementoFinal(actualPeli.darElemento());
					}
					gpa = gpa.darSiguiente();
				}
				generoDePeli = generoDePeli.darSiguiente();
			}
			actualPeli = actualPeli.darSiguiente();
		}
	}

	public void out()
	{
		NodoEncadenado<VOGeneroPelicula> actual = generos.darNodoPosicion(0);
		while(actual.darSiguiente() != null)
		{
			System.out.println("----------------------------------------------------------");
			System.out.println("G�NERO: " + actual.darElemento().getGenero());
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actual.darElemento().getPeliculas();
			NodoEncadenado<VOPelicula> actualPelicula = pelis.darNodoPosicion(0);
			System.out.println("Pel�culas: ");
			while(actualPelicula.darSiguiente() != null)
			{
				actualPelicula = actualPelicula.darSiguiente();
			}
			actual = actual.darSiguiente();
		}
	}
}
