package model.logic.Lectores;

import java.io.BufferedReader;
import java.io.FileReader;

import model.data_structures.NodoEncadenado;
import model.data_structures.Queue;
import model.vo.VORecomendacion;

public class LectorRecomendaciones 
{
	Queue<VORecomendacion> recomendaciones;
	public LectorRecomendaciones(String pRuta)
	{
		recomendaciones = leerArchivo(pRuta);
		leerArchivo(pRuta);
	}
	public Queue<VORecomendacion> leerArchivo(String pRuta)
	{
		Queue<VORecomendacion> recom = new Queue<VORecomendacion>();
		try
		{
			FileReader fr = new FileReader(pRuta);
			BufferedReader lector = new BufferedReader(fr);
			String lineaActual;
			lineaActual = lector.readLine();
			while(lineaActual != null)
			{
				try
				{
					String[] datos = lineaActual.split(",");
					long movieId = Long.parseLong(datos[0]);
					double rating = Double.parseDouble(datos[1]);
					VORecomendacion nueva = new VORecomendacion(movieId, rating);
					recomendaciones.enqueue(nueva);
				}
				catch(Exception e)
				{

				}
				lineaActual = lector.readLine();
			}
			lector.close();
			fr.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return recom;
	}
	
	public void out()
	{
		Queue<VORecomendacion> a = recomendaciones;
		while(a.isEmpty() == false)
		{
			System.out.println(a.dequeue().getRating());
		}
	}
	
	public Queue<VORecomendacion> darRecomendaciones()
	{
		return recomendaciones;
	}

}
