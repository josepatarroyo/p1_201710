package model.logic.comparadores;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparadorPeliculasTitulos implements Comparator<VOPelicula>
{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) 
	{	
		String s1 = o1.getTitulo();
		String s2 = o2.getTitulo();
		
		if(s1.compareTo(s2)>0)
		{
			return 1;
		}
		else if(s1.compareTo(s2)<0)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
