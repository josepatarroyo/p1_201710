package model.logic.comparadores;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparadorPeliculasTags implements Comparator<VOPelicula>
{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) 
	{
		int n1 = o1.getNumeroTags();
		int n2 = o2.getNumeroTags();
		if(n1 > n2)
		{
			return 1;
		}
		else if(n1 < n2)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	
}
