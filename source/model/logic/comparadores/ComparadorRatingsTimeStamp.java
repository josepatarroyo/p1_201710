package model.logic.comparadores;

import java.util.Comparator;

import model.vo.VORating;

public class ComparadorRatingsTimeStamp implements Comparator<VORating> 
{

	@Override
	public int compare(VORating o1, VORating o2) 
	{
		long t1 = o1.getTimeStamp();
		long t2 = o2.getTimeStamp();
		if(t1 > t2)
		{
			return 1;
		}
		else if(t1 < t2)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	
}
