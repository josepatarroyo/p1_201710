package model.logic.comparadores;

import java.util.Comparator;

import model.vo.VOUsuario;

public class ComparadorUsuariosTimestamp implements Comparator<VOUsuario>
{

	@Override
	public int compare(VOUsuario o1, VOUsuario o2) 
	{
		int n1 = o1.getNumRatings();
		int n2 = o2.getNumRatings();
		if(n1 > n2)
		{
			return 1;
		}
		else if(n1 < n2)
		{
			return -1;
		}
		else
		{
			if(n1 == n2)
			{
				long i1 = o1.getIdUsuario();
				long i2 = o2.getIdUsuario();
				if(i1 > i2)
				{
					return 1;
				}
				else
				{
					return -1;
				}
			}
		}
		return 0;
		
	}

}
