package model.logic.comparadores;

import java.util.Comparator;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.vo.VOPelicula;

public class ComparadorPeliculasRecomendacion implements Comparator<VOPelicula>
{
	private ListaEncadenada<VOPelicula> peliculas;
	
	public void  setPeliculas(ListaEncadenada<VOPelicula> l)
	{
		peliculas = l;
	}

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) 
	{
		if(contieneGenero(o1, o2) && rangoRating(o1, o2))
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
	
	private boolean contieneGenero(VOPelicula p1, VOPelicula p2)
	{
		boolean contiene = false;
		ListaEncadenada<String> generosAsociadosPelicula = (ListaEncadenada<String>) p2.getGenerosAsociados();
		NodoEncadenado<String> generoActual = generosAsociadosPelicula.darNodoPosicion(0);
		while(generoActual.darSiguiente() != null && !contiene)
		{
			if(p1.getGenerosAsociados().darElemento(0).equals(generoActual))
			{
				contiene = true;
			}
			generoActual = generoActual.darSiguiente();
		}
		return contiene;
	}
	
	private boolean rangoRating(VOPelicula p1, VOPelicula p2)
	{
		boolean dentro = false;
		double delta = 0.5;
		if(Math.abs(p1.getPromedioRatings() - p2.getPromedioRatings()) <= delta)
		{
			dentro = true;
		}
		return dentro;
	}

}
