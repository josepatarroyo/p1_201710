package model.logic.comparadores;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparadorPeliculasRatings implements Comparator<VOPelicula>
{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) 
	{
		double r1 = o1.getPromedioRatings();
		double r2 = o2.getPromedioRatings();
		
		if(r1 < r2)
		{
			return 1;
		}
		else if(r1 > r2)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}
	
}
