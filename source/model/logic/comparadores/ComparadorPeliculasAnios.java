package model.logic.comparadores;

import java.util.Comparator;

import model.vo.VOPelicula;

public class ComparadorPeliculasAnios implements Comparator<VOPelicula>
{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) 
	{
		int a1 = o1.getAgnoPublicacion();
		int a2 = o2.getAgnoPublicacion();
		if(a1 > a2)
		{
			return 1;
		}
		else if(a1 < a2)
		{
			return -1;
		}
		else
		{
			return 0;
		}
	}

}
