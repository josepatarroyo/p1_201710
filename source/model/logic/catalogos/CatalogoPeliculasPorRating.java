package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.logic.Merge;
import model.logic.comparadores.ComparadorPeliculasRatings;
import model.vo.VOPelicula;

public class CatalogoPeliculasPorRating 
{
	ListaEncadenada<VOPelicula> pelis;

	public CatalogoPeliculasPorRating(ListaEncadenada<VOPelicula> pelis)
	{
		this.pelis = pelis;
	}

	public void ordenarPorRating()
	{
		try
		{
			ComparadorPeliculasRatings c = new ComparadorPeliculasRatings();
			Merge.sort(pelis, c);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public ListaEncadenada<VOPelicula> darListaOrdenadaRatings()
	{
		return pelis;
	}
}
