package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.logic.Merge;
import model.logic.comparadores.ComparadorPeliculasAnios;
import model.logic.comparadores.ComparadorPeliculasTags;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOTag;

public class CatalogoGeneroPeliculaTags 
{
	private ListaEncadenada<VOGeneroPelicula> generos;
	private ListaEncadenada<VOTag> tags;

	public CatalogoGeneroPeliculaTags(ListaEncadenada<VOGeneroPelicula> generos, ListaEncadenada<VOTag> tags)
	{
		this.generos = generos;
		this.tags = tags;
		asociarTagsAPeliculas();
		ordenar();
		//out();
	}

	public void asociarTagsAPeliculas()
	{
		NodoEncadenado<VOGeneroPelicula> generoActual = generos.darNodoPosicion(0);
		NodoEncadenado<VOTag>  tagActual = tags.darNodoPosicion(0);
		while(tagActual.darSiguiente() != null)
		{
			
			while(generoActual.darSiguiente() != null)
			{
				
				ListaEncadenada<VOPelicula> peliculasGeneroActual = (ListaEncadenada<VOPelicula>) generoActual.darElemento().getPeliculas();
				NodoEncadenado<VOPelicula> peliculaActual = peliculasGeneroActual.darNodoPosicion(0);
				while(peliculaActual.darSiguiente() != null)
				{
					
					if(peliculaActual.darElemento().getId() == tagActual.darElemento().getMovieId())
					{
						System.out.println(peliculaActual.darElemento().getTitulo());
						if(peliculaActual.darElemento().getTagsAsociados() == null)
						{
							peliculaActual.darElemento().setTagsAsociados(new ListaEncadenada<String>());
						}
						peliculaActual.darElemento().getTagsAsociados().agregarElementoFinal(tagActual.darElemento().getTag());
					}
					peliculaActual = peliculaActual.darSiguiente();
				}
				generoActual = generoActual.darSiguiente();
			}
			tagActual = tagActual.darSiguiente();
		}
	}

	public void ordenar()
	{
		try
		{
			ComparadorPeliculasAnios c = new ComparadorPeliculasAnios();
			NodoEncadenado<VOGeneroPelicula> generoActual = generos.darNodoPosicion(0);
			while(generoActual.darSiguiente() != null)
			{
				ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) generoActual.darElemento().getPeliculas();
				pelis = (ListaEncadenada<VOPelicula>) Merge.sort(pelis, c);
				generoActual.darElemento().setPeliculas(pelis);
				generoActual = generoActual.darSiguiente();
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void out()
	{
		NodoEncadenado<VOGeneroPelicula> actualgenero = generos.darNodoPosicion(0);
		while(actualgenero.darSiguiente() != null)
		{
			System.out.println("GENERO: " + actualgenero.darElemento().getGenero());
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actualgenero.darElemento().getPeliculas();
			NodoEncadenado<VOPelicula> actualpeli = pelis.darNodoPosicion(0);
			while(actualpeli.darSiguiente() != null)
			{
				System.out.println(actualpeli.darElemento().getAgnoPublicacion());
				actualpeli = actualpeli.darSiguiente();
			}
			actualgenero = actualgenero.darSiguiente();
		}
	}

	public ListaEncadenada<VOGeneroPelicula> darGeneros()
	{
		return generos;
	}

	private ListaEncadenada<VOGeneroPelicula> ordenarPorTags()
	{
		ComparadorPeliculasTags c = new ComparadorPeliculasTags();
		ListaEncadenada<VOGeneroPelicula> generitos = generos;
		NodoEncadenado<VOGeneroPelicula> actualGenero = generitos.darNodoPosicion(0);
		while(actualGenero.darSiguiente() != null)
		{
			ListaEncadenada<VOPelicula> pelisActual = (ListaEncadenada<VOPelicula>) actualGenero.darElemento().getPeliculas();
			try
			{
				Merge.sort(pelisActual, c);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			actualGenero = actualGenero.darSiguiente();
		}
		return generitos;
	}
	
	public ListaEncadenada<VOGeneroPelicula> darListaGenerosPeliculasMasTags(int n)
	{
		ListaEncadenada<VOGeneroPelicula> generitos = ordenarPorTags();
		NodoEncadenado<VOGeneroPelicula> actualGenero = generitos.darNodoPosicion(0);
		while(actualGenero.darSiguiente() != null)
		{
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actualGenero.darElemento().getPeliculas();
			pelis.darNodoPosicion(n-1).cambiarSiguiente(null);
			actualGenero = actualGenero.darSiguiente();
		}
		return generitos;
	}

}
