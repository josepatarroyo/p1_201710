package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.logic.Merge;
import model.logic.comparadores.ComparadorRatingsTimeStamp;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VORating;

public class CatalogoPeliculaRatingsPorTimestamp 
{
	ListaEncadenada<VOGeneroPelicula> generos;
	ListaEncadenada<VORating> ratings;
	public CatalogoPeliculaRatingsPorTimestamp(ListaEncadenada<VOGeneroPelicula> generos)
	{
		this.generos = generos;
		ratings = new ListaEncadenada<VORating>();
	}

	private VOPelicula buscarPeli(long id)
	{
		NodoEncadenado<VOGeneroPelicula> actualGenero = generos.darNodoPosicion(0);
		while(actualGenero.darSiguiente() != null)
		{
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actualGenero.darElemento().getPeliculas();
			NodoEncadenado<VOPelicula> peliActual = pelis.darNodoPosicion(0);
			while(peliActual.darSiguiente() != null)
			{
				if(peliActual.darElemento().getId() == id)
				{
					return peliActual.darElemento();
				}
				peliActual = peliActual.darSiguiente();
			}
			actualGenero = actualGenero.darSiguiente();
		}
		return null;
	}

	public ListaEncadenada<VORating> darListaRatingsPeli(long id)
	{
		try
		{
			ComparadorRatingsTimeStamp c = new ComparadorRatingsTimeStamp();
			ratings = (ListaEncadenada<VORating>) buscarPeli(id).getRatings();
			if(ratings != null && ratings.darNumeroElementos() > 0)
			{
				ratings = (ListaEncadenada<VORating>) Merge.sort(ratings, c);
				out();
			}
			
			return ratings;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return null;
	}

	public void out()
	{
		NodoEncadenado<VORating> ratingActual = ratings.darNodoPosicion(0);
		while(ratingActual.darSiguiente() != null)
		{
			System.out.println(ratingActual.darElemento().getRating());
			ratingActual = ratingActual.darSiguiente();
		}
	}
}
