package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.vo.VORating;
import model.vo.VOUsuario;

public class CatalogoUsuariosRatings
{
	private ListaEncadenada<VOUsuario> usuarios;
	private ListaEncadenada<VORating> ratings;
	
	public CatalogoUsuariosRatings(ListaEncadenada<VOUsuario> usuarios, ListaEncadenada<VORating> ratings)
	{
		this.usuarios = usuarios;
		this.ratings = ratings;
		asociar();
	}
	
	public void asociar()
	{
		NodoEncadenado<VORating> ratingActual = ratings.darNodoPosicion(0);
		while(ratingActual.darSiguiente() != null)
		{
			boolean agregado = false;
			NodoEncadenado<VOUsuario> usuarioActual = usuarios.darNodoPosicion(0);
			while(usuarioActual.darSiguiente() != null && !agregado)
			{
				if(ratingActual.darElemento().getIdUsuario() == usuarioActual.darElemento().getIdUsuario())
				{
					if(usuarioActual.darElemento().getPrimerTimestamp() == 0)
					{
						usuarioActual.darElemento().setPrimerTimestamp(ratingActual.darElemento().getTimeStamp());
					}
					usuarioActual.darElemento().sumarRating();
					agregado = true;
				}
				usuarioActual = usuarioActual.darSiguiente();
			}
			ratingActual = ratingActual.darSiguiente();
		}
	}
	
	public ListaEncadenada<VOUsuario> darListaUsuarios()
	{
		return usuarios;
	}
}
