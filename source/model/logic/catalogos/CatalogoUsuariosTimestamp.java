package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.logic.Merge;
import model.logic.comparadores.ComparadorUsuariosTimestamp;
import model.vo.VOUsuario;

public class CatalogoUsuariosTimestamp 
{
	private ListaEncadenada<VOUsuario> usuarios;
	public CatalogoUsuariosTimestamp(ListaEncadenada<VOUsuario> usuarios)
	{
		this.usuarios = usuarios;
	}

	public ListaEncadenada<VOUsuario> darListaUsuarios()
	{
		try
		{
			ComparadorUsuariosTimestamp c = new ComparadorUsuariosTimestamp();
			Merge.sort(usuarios, c);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return usuarios;
	}
}
