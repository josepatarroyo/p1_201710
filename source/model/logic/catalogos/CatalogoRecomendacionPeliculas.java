package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.data_structures.Queue;
import model.logic.Lectores.LectorRecomendaciones;
import model.logic.comparadores.ComparadorPeliculasRecomendacion;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VORecomendacion;

public class CatalogoRecomendacionPeliculas 
{
	private ListaEncadenada<VOGeneroPelicula> generos;
	private Queue<VORecomendacion> lecturaArchivo;
	private ListaEncadenada<VORecomendacion> peliculasDeArchivoRecomendacion;
	private ListaEncadenada<VOPeliculaPelicula> recomendacion;
	private ListaEncadenada<VOPelicula> pelis;

	public CatalogoRecomendacionPeliculas(ListaEncadenada<VOGeneroPelicula> generos, ListaEncadenada<VOPelicula> pelis)
	{
		this.generos = generos;
		this.pelis = pelis;
		leerArchivo("data/recomendaciones.csv");
		generarListaPeliculasPrincipalesRecomendacion();
		agregarPeliculasRelacionadasRecomendacion();
		out();
	}

	public void leerArchivo(String pRuta)
	{
		LectorRecomendaciones l = new LectorRecomendaciones(pRuta);
		lecturaArchivo = l.darRecomendaciones();
	}

	public void generarListaPeliculasPrincipalesRecomendacion()
	{
		while(!lecturaArchivo.isEmpty())
		{
			if(peliculasDeArchivoRecomendacion == null)
			{
				peliculasDeArchivoRecomendacion = new ListaEncadenada<VORecomendacion>();
			}
			VORecomendacion nueva = lecturaArchivo.dequeue();
			peliculasDeArchivoRecomendacion.agregarElementoFinal(nueva);
		}

		NodoEncadenado<VORecomendacion> actualRecomendacion = peliculasDeArchivoRecomendacion.darNodoPosicion(0);
		while(actualRecomendacion.darSiguiente() != null)
		{
			boolean encontrado = false;
			long idMovie = actualRecomendacion.darElemento().getIdMovie();
			NodoEncadenado<VOPelicula> actualPelicula = pelis.darNodoPosicion(0);
			while(actualPelicula != null && !encontrado)
			{
				if(actualPelicula.darElemento().getId() == idMovie)
				{
					encontrado = true;
					VOPeliculaPelicula nueva = new VOPeliculaPelicula();
					nueva.setPelicula(actualPelicula.darElemento());
					if(recomendacion == null)
					{
						recomendacion = new ListaEncadenada<VOPeliculaPelicula>();
					}
					recomendacion.agregarElementoFinal(nueva);
				}
				actualPelicula = actualPelicula.darSiguiente();
			}
			actualRecomendacion = actualRecomendacion.darSiguiente();
		}
	}

	public void agregarPeliculasRelacionadasRecomendacion()
	{
		ComparadorPeliculasRecomendacion c = new ComparadorPeliculasRecomendacion();
		NodoEncadenado<VOPeliculaPelicula> actualPeliculaPelicula = recomendacion.darNodoPosicion(0);
		while(actualPeliculaPelicula.darSiguiente() != null)
		{
			String generoBuscado = actualPeliculaPelicula.darElemento().getPelicula().getGeneroPrincipal();
			NodoEncadenado<VOGeneroPelicula> actualGenero = generos.darNodoPosicion(0);
			boolean generoEncontrado = false;
			while(actualGenero.darSiguiente() != null && !generoEncontrado)
			{
				if(actualGenero.darElemento().getGenero().equals(generoBuscado))
				{
					generoEncontrado = true;
					ListaEncadenada<VOPelicula> peliculasRelacionadas = (ListaEncadenada<VOPelicula>) actualGenero.darElemento().getPeliculas();
					NodoEncadenado<VOPelicula> peli = peliculasRelacionadas.darNodoPosicion(0);
					while(peli.darSiguiente() != null)
					{
						int agregar = c.compare(actualPeliculaPelicula.darElemento().getPelicula(), peli.darElemento());
						if(agregar == 1)
						{
							actualPeliculaPelicula.darElemento().getPeliculasRelacionadas().agregarElementoFinal(peli.darElemento());
						}
						peli = peli.darSiguiente();
					}
				}
				actualGenero = actualGenero.darSiguiente();
			}
			actualPeliculaPelicula = actualPeliculaPelicula.darSiguiente();
		}
	}

	public void out()
	{
		NodoEncadenado<VOPeliculaPelicula> actualR = recomendacion.darNodoPosicion(0);
		while(actualR.darSiguiente() != null)
		{
			System.out.println("-----------------------------------------------------------------");
			System.out.println("PELICULA PRINCIPAL: " + actualR.darElemento().getPelicula().getTitulo() + " - " + actualR.darElemento().getPelicula().getGeneroPrincipal());
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actualR.darElemento().getPeliculasRelacionadas();
			if(pelis != null && pelis.darNumeroElementos() > 0)
			{
				NodoEncadenado<VOPelicula> actualPeli = pelis.darNodoPosicion(0);
				while(actualPeli.darSiguiente() != null)
				{
					System.out.println(actualPeli.darElemento().getGeneroPrincipal());
					actualPeli = actualPeli.darSiguiente();
				}
			}
			actualR = actualR.darSiguiente();
		}
	}

	public ListaEncadenada<VOPeliculaPelicula> darListaRecomendaciones()
	{
		return recomendacion;
	}

}
