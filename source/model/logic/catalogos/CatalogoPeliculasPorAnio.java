package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.logic.Merge;
import model.logic.comparadores.ComparadorPeliculasAnios;
import model.vo.VOPelicula;

public class CatalogoPeliculasPorAnio 
{
	ListaEncadenada<VOPelicula> pelis;

	public CatalogoPeliculasPorAnio(ListaEncadenada<VOPelicula> pelis)
	{
		this.pelis = pelis;
		ordenarPorAnio();
	}

	public void ordenarPorAnio()
	{
		ComparadorPeliculasAnios c = new ComparadorPeliculasAnios();
		try
		{
			pelis = (ListaEncadenada<VOPelicula>) Merge.sort(pelis, c);
			out();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void out()
	{
		System.out.println("ENTRA");
		NodoEncadenado<VOPelicula> actualPelicula = pelis.darNodoPosicion(0);
		System.out.println(actualPelicula.darSiguiente().darElemento().getTitulo());
		while(actualPelicula.darSiguiente() != null)
		{
			System.out.println(actualPelicula.darElemento().getTitulo() + " - " + actualPelicula.darElemento().getAgnoPublicacion());
			actualPelicula = actualPelicula.darSiguiente();
		}
	}
	
	public ListaEncadenada<VOPelicula> darListaOrdenadaPorAnio()
	{
		return pelis;
	}
}
