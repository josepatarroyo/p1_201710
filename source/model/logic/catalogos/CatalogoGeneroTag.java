package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOPelicula;
import model.vo.VOTag;

public class CatalogoGeneroTag 
{
	private ListaEncadenada<VOGeneroPelicula> generos;
	private ListaEncadenada<VOGeneroTag> generosTags;
	public CatalogoGeneroTag(ListaEncadenada<VOGeneroPelicula> generos)
	{
		this.generos = generos;
	}
	
	private void enlazarGeneros()
	{
		generosTags = new ListaEncadenada<VOGeneroTag>();
		NodoEncadenado<VOGeneroPelicula> actualGenero = generos.darNodoPosicion(0);
		while(actualGenero.darSiguiente() != null)
		{
			VOGeneroTag nuevo = new VOGeneroTag();
			nuevo.setGenero(actualGenero.darElemento().getGenero());
			actualGenero = actualGenero.darSiguiente();
		}
	}
	
	public ListaEncadenada<VOGeneroTag> enlazarTags()
	{
		enlazarGeneros();
		NodoEncadenado<VOGeneroPelicula> actualGeneroPelicula = generos.darNodoPosicion(0);
		NodoEncadenado<VOGeneroTag> actualGeneroTag = generosTags.darNodoPosicion(0);
		while(actualGeneroPelicula.darSiguiente() != null)
		{
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actualGeneroPelicula.darElemento().getPeliculas();
			NodoEncadenado<VOPelicula> actualPelicula = pelis.darNodoPosicion(0);
			while(actualPelicula.darSiguiente() != null)
			{
				ListaEncadenada<String> tags = (ListaEncadenada<String>) actualPelicula.darElemento().getTagsAsociados();
				NodoEncadenado<String> actualTag = tags.darNodoPosicion(0);
				{
					while(actualTag.darSiguiente() != null)
					{
						actualGeneroTag.darElemento().getTags().agregarElementoFinal(actualTag.darElemento());
					}
				}
			}
			actualGeneroPelicula = actualGeneroPelicula.darSiguiente();
			actualGeneroTag = actualGeneroTag.darSiguiente();
		}
		return generosTags;
	}
	
	
}
