package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.logic.Merge;
import model.logic.comparadores.ComparadorPeliculasRatings;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;
import model.vo.VORating;

public class CatalogoGeneroPeliculas 
{
	private ListaEncadenada<VOGeneroPelicula> generos;

	public CatalogoGeneroPeliculas(ListaEncadenada<VOGeneroPelicula> generos)
	{
		System.out.println("CONS");
		this.generos = generos;
		ordenarPopularesGenero();
	}


	public ListaEncadenada<VOGeneroPelicula> darGenerosYSusPeliculas()
	{
		return generos;
	}

	public void ordenarPopularesGenero()
	{
		System.out.println("ORD");
		ComparadorPeliculasRatings c = new ComparadorPeliculasRatings();
		NodoEncadenado<VOGeneroPelicula> actual = generos.darNodoPosicion(0);
		while(actual.darSiguiente() != null)
		{
			try
			{
				ListaEncadenada<VOPelicula> temp = (ListaEncadenada<VOPelicula>) actual.darElemento().getPeliculas();
				temp = (ListaEncadenada<VOPelicula>) Merge.sort(temp, c);
				actual.darElemento().setPeliculas(temp);
			}
			catch(Exception e )
			{
				e.printStackTrace();
			}
			actual = actual.darSiguiente();
		}
		System.out.println("FORD");
	}

	public ListaEncadenada<VOGeneroPelicula> darNPopularesPorGenero(int n)
	{
		ListaEncadenada<VOGeneroPelicula> respuesta = generos;
		NodoEncadenado<VOGeneroPelicula> actualGenero = respuesta.darNodoPosicion(0);
		while(actualGenero.darSiguiente() != null)
		{
			//System.out.println("GENERO: " + actualGenero.darElemento().getGenero());
			ListaEncadenada<VOPelicula> aux = new ListaEncadenada<VOPelicula>();
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actualGenero.darElemento().getPeliculas();
			if(pelis.darNumeroElementos() < n)
			{
				n = pelis.darNumeroElementos();
			}
			for(int i=0; i < n; i++)
			{
				
				if(pelis.darElemento(i) != null)
				{
					//System.out.println("PELI: " + pelis.darElemento(i).getTitulo());
					aux.agregarElementoFinal(pelis.darElemento(i));
				}
			}
			actualGenero.darElemento().setPeliculas(aux);
			actualGenero = actualGenero.darSiguiente();
		}
		return respuesta;
	}

	public void out()
	{
		NodoEncadenado<VOGeneroPelicula> actualGenero = generos.darNodoPosicion(0);
		while(actualGenero.darSiguiente() != null)
		{
			//System.out.println("GENERO: " + actualGenero.darElemento().getGenero());
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actualGenero.darElemento().getPeliculas();
			NodoEncadenado<VOPelicula> actualPeli = pelis.darNodoPosicion(0);
			while(actualPeli.darSiguiente() != null)
			{
				//System.out.println("PELI: " + actualPeli.darElemento().getTitulo() + " - " + actualPeli.darElemento().getNumeroRatings());
				actualPeli = actualPeli.darSiguiente();
			}
			actualGenero = actualGenero.darSiguiente();
		}
	}

}
