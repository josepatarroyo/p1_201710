package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.vo.VOPelicula;
import model.vo.VORating;

public class CatalogoPeliculaRating 
{
	public CatalogoPeliculaRating(ListaEncadenada<VOPelicula> pelis, ListaEncadenada<VORating> ratings)
	{
		
	}
	
	public void enlazarRatings(ListaEncadenada<VOPelicula> pelis,ListaEncadenada<VORating> ratings)
	{
		NodoEncadenado<VORating> ratingActual = ratings.darNodoPosicion(0);
		while(ratingActual.darSiguiente() != null)
		{
			boolean agregado = false;
			NodoEncadenado<VOPelicula> actualPelicula = pelis.darNodoPosicion(0);
			while(actualPelicula.darSiguiente() != null && !agregado)
			{
				if(actualPelicula.darElemento().getId() == ratingActual.darElemento().getIdPelicula())
				{
					actualPelicula.darElemento().agregarRating(ratingActual.darElemento());
					agregado = true;
				}
				actualPelicula.darElemento().promedio();
				actualPelicula = actualPelicula.darSiguiente();
			}
			ratingActual = ratingActual.darSiguiente();
		}
	}
}
