package model.logic.catalogos;

import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.logic.Merge;
import model.logic.comparadores.ComparadorPeliculasRatings;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class CatalogoGeneroPeliculasPorRating 
{
	private ListaEncadenada<VOGeneroPelicula> generos;

	public CatalogoGeneroPeliculasPorRating(ListaEncadenada<VOGeneroPelicula> generos)
	{
		this.generos = generos;
		ordenarPorRating();
		out();
	}

	public void ordenarPorRating()
	{
		System.out.println("ENTRA");
		NodoEncadenado<VOGeneroPelicula> actual = generos.darNodoPosicion(0);
		ComparadorPeliculasRatings c = new ComparadorPeliculasRatings();
		while(actual.darSiguiente() != null)
		{
			ListaEncadenada<VOPelicula> peliculas = (ListaEncadenada<VOPelicula>) actual.darElemento().getPeliculas();
			try
			{
				peliculas = (ListaEncadenada<VOPelicula>) Merge.sort(peliculas, c);
				actual.darElemento().setPeliculas(peliculas);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
			actual = actual.darSiguiente();
		}
		
	}

	public void out()
	{
		NodoEncadenado<VOGeneroPelicula> actual = generos.darNodoPosicion(0);
		while(actual.darSiguiente() != null)
		{
			System.out.println("GENERO: " + actual.darElemento().getGenero());
			ListaEncadenada<VOPelicula> pelis = (ListaEncadenada<VOPelicula>) actual.darElemento().getPeliculas();
			NodoEncadenado<VOPelicula> actualPelicula = pelis.darNodoPosicion(0);
			
			
			while(actualPelicula.darSiguiente() != null)
			{
				
				System.out.println(actualPelicula.darElemento().getTitulo() + actualPelicula.darElemento().getPromedioRatings());
				actualPelicula = actualPelicula.darSiguiente();
			}
			actual = actual.darSiguiente();
		}
	}


	public ListaEncadenada<VOGeneroPelicula> darGeneros()
	{
		return generos;
	}

}
