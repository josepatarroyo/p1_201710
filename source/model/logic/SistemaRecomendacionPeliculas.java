package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;
import model.data_structures.Queue;
import model.logic.Lectores.LectorDePeliculasYGeneros;
import model.logic.Lectores.LectorRatings;
import model.logic.Lectores.LectorTags;
import model.logic.catalogos.CatalogoGeneroPeliculaTags;
import model.logic.catalogos.CatalogoGeneroPeliculas;
import model.logic.catalogos.CatalogoGeneroPeliculasPorRating;
import model.logic.catalogos.CatalogoGeneroTag;
import model.logic.catalogos.CatalogoPeliculaRating;
import model.logic.catalogos.CatalogoPeliculaRatingsPorTimestamp;
import model.logic.catalogos.CatalogoPeliculasPorAnio;
import model.logic.catalogos.CatalogoPeliculasPorRating;
import model.logic.catalogos.CatalogoRecomendacionPeliculas;
import model.logic.catalogos.CatalogoUsuariosTimestamp;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas
{
	//Ruta archivos
	public static final String RUTA_PELICULAS = "data/movies.csv";
	public static final String RUTA_RATINGS = "data/ratings.csv";
	public static final String RUTA_TAGS = "data/tags.csv";
	public static final String RUTA_RECOMENDACIONES = "data/recomendaciones.csv";

	//Listas crudas
	private ListaEncadenada<VOPelicula> pelis;
	private ListaEncadenada<VOGeneroPelicula> generos;
	private ListaEncadenada<VORating> ratings;
	private ListaEncadenada<VOTag> tags;
	private ListaEncadenada<VOUsuario> usuarios;
	private Queue<VOOperacion> operaciones;

	//Cat�logos
	private CatalogoGeneroPeliculas catalogoGeneroPeliculas;
	private CatalogoPeliculasPorAnio catalogoPeliculasPorAnio;
	private CatalogoGeneroPeliculasPorRating catalogoGeneroPeliculasPorRating;
	private CatalogoGeneroPeliculaTags catalogoGeneroPeliculaTags;
	private CatalogoRecomendacionPeliculas catalogoRecomendacionPeliculas;
	private CatalogoPeliculaRating catalogoPeliculaRating;
	private CatalogoPeliculaRatingsPorTimestamp catalogoPeliculaRatingsPorTimeStamp;


	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) 
	{				
		LectorDePeliculasYGeneros l = new LectorDePeliculasYGeneros(RUTA_PELICULAS);
		pelis = l.darPeliculas();
		generos = l.darGeneros();
		if(pelis != null && generos != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	@Override
	public boolean cargarRatingsSR(String rutaRatings) 
	{
		LectorRatings l = new LectorRatings(rutaRatings);
		ratings = l.darRatings();
		usuarios = l.darUsuarios();
		if(ratings != null && usuarios != null)
		{
			return true;
		}
		else
		{
			return false;
		}

	}

	private void asociarRatingsPeliculas()
	{
		AsociadorRatingsPeliculas a = new AsociadorRatingsPeliculas(pelis, ratings, generos);
		generos = a.darGeneros();
	}

	@Override
	public boolean cargarTagsSR(String rutaTags) 
	{
		LectorTags l = new LectorTags(rutaTags);
		tags = new ListaEncadenada<VOTag>();
		tags = l.darTags();
		if(tags != null)
		{
			return true;
		}
		else
		{
			return false;
		}
	}


	@Override
	public int sizeMoviesSR() 
	{
		return pelis.darNumeroElementos();
	}

	@Override
	public int sizeUsersSR() 
	{
		return usuarios.darNumeroElementos();
	}

	@Override
	public int sizeTagsSR() 
	{
		return tags.darNumeroElementos();
	}

	@Override
	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) 
	{
		catalogoGeneroPeliculas = new CatalogoGeneroPeliculas(generos);
		return catalogoGeneroPeliculas.darNPopularesPorGenero(n);
	}

	@Override
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() 
	{
		catalogoPeliculasPorAnio = new CatalogoPeliculasPorAnio(pelis);
		return catalogoPeliculasPorAnio.darListaOrdenadaPorAnio();
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarGeneroSR() 
	{
		catalogoGeneroPeliculasPorRating = new CatalogoGeneroPeliculasPorRating(generos);
		return catalogoGeneroPeliculasPorRating.darGeneros();
	}

	@Override
	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() 
	{
		catalogoGeneroPeliculaTags = new CatalogoGeneroPeliculaTags(generos, tags);
		return catalogoGeneroPeliculaTags.darGeneros();
	}

	@Override
	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) 
	{
		catalogoRecomendacionPeliculas = new CatalogoRecomendacionPeliculas(generos, pelis);
		catalogoRecomendacionPeliculas.leerArchivo(rutaRecomendacion);
		return catalogoRecomendacionPeliculas.darListaRecomendaciones();
	}

	@Override
	public ILista<VORating> ratingsPeliculaSR(long idPelicula) 
	{
		catalogoPeliculaRatingsPorTimeStamp = new CatalogoPeliculaRatingsPorTimestamp(generos);
		return catalogoPeliculaRatingsPorTimeStamp.darListaRatingsPeli(idPelicula);
	}

	//--------------------------------------------------------------------------------
	// PARTE B
	//--------------------------------------------------------------------------------
	@Override
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() 
	{
		return null;	
	}

	@Override
	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) 
	{
		return null;
	}

	@Override
	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() 
	{
		return null;
	}

	@Override
	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub
		return null;
	}

	//------------------------------------------------------------------------------
	// PARTE C
	//------------------------------------------------------------------------------

	@Override
	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) 
	{
		if(operaciones == null)
		{
			operaciones = new Queue<VOOperacion>();
		}
		else
		{
			VOOperacion nueva = new VOOperacion();
			nueva.setOperacion(nomOperacion);
			nueva.setTimestampInicio(tinicio);
			nueva.setTimestampFin(tfin);
			operaciones.enqueue(nueva);
		}

	}

	@Override
	public ILista<VOOperacion> darHistoralOperacionesSR() 
	{
		Queue<VOOperacion> temp = operaciones;
		Queue<VOOperacion> aux = new Queue<VOOperacion>();
		while(!temp.isEmpty())
		{
			aux.enqueue(temp.dequeue());
		}
		ListaEncadenada<VOOperacion> listaOperaciones = new ListaEncadenada<VOOperacion>();
		for(int i=0; i < temp.size(); i++)
		{
			listaOperaciones.agregarElementoFinal(aux.dequeue());
		}
		return listaOperaciones;
	}

	@Override
	public void limpiarHistorialOperacionesSR() 
	{
		operaciones = new Queue<VOOperacion>();
	}

	@Override
	public ILista<VOOperacion> darUltimasOperaciones(int n) 
	{
		Queue<VOOperacion> temp = operaciones;
		Queue<VOOperacion> aux = new Queue<VOOperacion>();
		while(!temp.isEmpty())
		{
			aux.enqueue(temp.dequeue());
		}
		ListaEncadenada<VOOperacion> listaOperaciones = new ListaEncadenada<VOOperacion>();
		for(int i=0; i < n ; i++)
		{
			listaOperaciones.agregarElementoFinal(aux.dequeue());
		}
		return listaOperaciones;
	}

	@Override
	public void borrarUltimasOperaciones(int n) 
	{
		for(int i=0; i < n; i++)
		{
			operaciones.dequeue();
		}
	}

	@Override
	public long agregarPelicula(String titulo, int agno, String[] generos) 
	{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void agregarRating(int idUsuario, int idPelicula, double rating) 
	{
		// TODO Auto-generated method stub

	}

	//------------------------------------------------
	// M�todos Auxiliares
	//------------------------------------------------
	public ListaEncadenada<VOPelicula> darPelis()
	{
		return pelis;
	}

	public ListaEncadenada<VOGeneroPelicula> darGeneros()
	{
		return generos;
	}

	public ListaEncadenada<VORating> darRatings()
	{
		return ratings;
	}
	public CatalogoPeliculaRating darCatalogoPeliculaRating()
	{
		return catalogoPeliculaRating;
	}
	public CatalogoGeneroPeliculas darCatalogoGeneroPeliculas()
	{
		return catalogoGeneroPeliculas;
	}

	public static void main(String[] args)
	{
		SistemaRecomendacionPeliculas s = new SistemaRecomendacionPeliculas();
		System.out.println("PELICULAS Y GENEROS: " + s.cargarPeliculasSR(RUTA_PELICULAS));
		System.out.println("RATINGS: " + s.cargarRatingsSR(RUTA_RATINGS));
		System.out.println("TAGS: " + s.cargarTagsSR(RUTA_TAGS));
		s.asociarRatingsPeliculas();
		//s.peliculasPopularesSR(5);
		//s.catalogoPeliculasOrdenadoSR();
		//s.recomendarGeneroSR();
		//s.opinionRatingsGeneroSR();
		s.recomendarPeliculasSR(RUTA_RECOMENDACIONES, 2);
		s.ratingsPeliculaSR(1);
	}

}
