package model.logic;

import model.data_structures.ListaEncadenada;
import model.vo.VOGeneroPelicula;
import model.vo.VOUsuario;

public class AsociadorUsuariosGeneros 
{
	private ListaEncadenada<VOGeneroPelicula> generos;
	private ListaEncadenada<VOUsuario> usuarios;
	
	public AsociadorUsuariosGeneros(ListaEncadenada<VOGeneroPelicula> generos , ListaEncadenada<VOUsuario> usuarios)
	{
		this.generos = generos;
		this.usuarios = usuarios;
	}
	
	public void asociar()
	{
		
	}
}
