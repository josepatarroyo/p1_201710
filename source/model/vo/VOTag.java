package model.vo;

public class VOTag 
{
	private long userId;
	private long movieId;
	private String tag;
	private long timestamp;
	
	public String getTag() 
	{
		return tag;
	}
	public void setTag(String tag) 
	{
		this.tag = tag;
	}
	public void setUserId(long userId)
	{
		this.userId = userId;
	}
	public void setMovieId(long movieId)
	{
		this.movieId = movieId;
	}
	public long getUserId()
	{
		return userId;
	}
	public long getMovieId()
	{
		return movieId;
	}
	public long getTimestamp() 
	{
		return timestamp;
	}
	public void setTimestamp(long timestamp) 
	{
		this.timestamp = timestamp;
	}
	
}
