package model.vo;

public class VORecomendacion 
{
	private long idMovie;
	private double rating;
	
	public VORecomendacion(long idMovie, double rating)
	{
		this.idMovie = idMovie;
		this.rating = rating;
	}
	public long getIdMovie()
	{
		return idMovie;
	}
	public double getRating()
	{
		return rating;
	}
}
