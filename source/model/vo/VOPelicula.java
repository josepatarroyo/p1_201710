package model.vo;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoEncadenado;

public class VOPelicula {
	
	private long id;
	private String titulo;
	private int numeroRatings;
	private int numeroTags;
	private double promedioRatings;
	private int agnoPublicacion;
	private ILista<String> tagsAsociados;
	private ILista<String> generosAsociados;
	private ILista<VORating> ratings;
	private String generoPrincipal;
	
	public String getTitulo() 
	{
		return titulo;
	}
	public void setId(long id)
	{
		this.id = id;
	}
	public long getId()
	{
		return id;
	}
	public void setTitulo(String titulo) 
	{
		this.titulo = titulo;
	}
	public String getGeneroPrincipal()
	{
		return getGenerosAsociados().darElemento(0);
	}
	public int getNumeroRatings() {
		return numeroRatings;
	}
	public void setNumeroRatings(int numeroRatings) {
		this.numeroRatings = numeroRatings;
	}
	public int getNumeroTags() {
		return numeroTags;
	}
	public void setNumeroTags(int numeroTags) {
		this.numeroTags = numeroTags;
	}
	public double getPromedioRatings() 
	{
		return promedioRatings;
	}
	public void setPromedioRatings(double promedioRatings) {
		this.promedioRatings = promedioRatings;
	}
	public int getAgnoPublicacion() {
		return agnoPublicacion;
	}
	public void setAgnoPublicacion(int agnoPublicacion) {
		this.agnoPublicacion = agnoPublicacion;
	}
	public ILista<String> getTagsAsociados() {
		return tagsAsociados;
	}
	public void setTagsAsociados(ILista<String> tagsAsociados) {
		this.tagsAsociados = tagsAsociados;
	}
	public ILista<String> getGenerosAsociados() {
		return generosAsociados;
	}
	public void setGenerosAsociados(ILista<String> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}
	public void agregarRating(VORating rating)
	{
		if(ratings ==  null)
		{
			ratings = new ListaEncadenada<VORating>();
		}
		ratings.agregarElementoFinal(rating);
	}
	public void promedio()
	{
		double suma = 0;
		double contador = 0;
		ListaEncadenada<VORating> ratings = (ListaEncadenada<VORating>) this.ratings;
		NodoEncadenado<VORating> ratingActual = ratings.darNodoPosicion(0);
		while(ratingActual.darSiguiente() != null)
		{
			suma = suma + ratingActual.darElemento().getRating();
			contador++;
			ratingActual = ratingActual.darSiguiente();
		}
		setPromedioRatings(suma/contador);
	}
	public ILista<VORating> getRatings()
	{
		return ratings;
	}
	

}
